We begin by learning your exercise background, current fitness level, injuries/surgeries and goals, then build a customized plan for you. Your visits in our studio are private, scheduled appointments. Nutrition counseling is available through a private meeting with our licensed dietician.

Address: 2959 Canfield Road, Suite 12, Youngstown, OH 44511, USA

Phone: 330-718-8009

Website: https://axiofitness.com
